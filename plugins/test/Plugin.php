<?php
namespace plugins\test;

class Plugin extends \core\base\Plugin
{
    public function install()
    {
        echo 'install';
    }

    public function uninstall()
    {
        echo 'uninstall';
    }

    public function activate()
    {
        echo 'activate';
    }

    public function deactivate()
    {
        echo 'deactivate';
    }

    public function load()
    {
        echo 'load';
    }
}
