<?php

namespace qiniu;


class Util
{
    public static function decode($str)
    {
        return \base64_decode(\str_replace(
            ['-', '_'],
            ['+', '/'],
            $str
        ));
    }

    public static function encode($str)
    {
        return \str_replace([
            '+', '/',
        ], ['-', '_'], \base64_encode($str));
    }
} 