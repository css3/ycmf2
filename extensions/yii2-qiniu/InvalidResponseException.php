<?php

namespace qiniu;


use yii\base\Exception;

class InvalidResponseException extends Exception
{
    public $responseHeaders = [];

    public $responseBody = '';

    public function __construct($responseHeaders, $responseBody, $message = null, $code = 0, \Exception $previous = null)
    {
        $this->responseHeaders = $responseHeaders;
        $this->responseBody = $responseBody;
        parent::__construct($message, $code);
    }
} 