#qiniu

##Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist htmlcook/yii2-qiniu "*"
```

or add

```
"htmlcook/yii2-qiniu": "*"
```

to the require section of your `composer.json` file.


##Usage

