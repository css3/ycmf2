<?php
Yii::setAlias('uploads', dirname(dirname(__DIR__)) . '/uploads');
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['pluginManager'],
    'aliases' => [
        '@qiniu' => '@ext/yii2-qiniu',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'option' => [
            'class' => 'common\components\Option',
        ],
        'pluginManager' => [
            'class' => 'common\base\PluginManager',
        ],
        'qiniu' => [
            'class' => 'qiniu\Qiniu',
        ]
    ],
    'params' => [
        'settings' => require __DIR__ . '/default-settings.php',
    ],
];
