<?php
return [
    'general' => [
        'title' => '常规选项',
        'options' => [
            'site_title' => [
                'label' => '站点标题',
                'require' => true,
            ],
            'site_keywords' => [
                'label' => '站点关键字',
                'hint' => '用英文逗号<code>,</code>分隔开来。',
            ],
            'site_description' => [
                'label' => '站点描述',
                'type' => 'textarea',
                'hint' => '长度不要超过255个字符。',
                'inputOptions' => ['rows' => 3],
                'rule' => [
                    'string' => ['max' => 255],
                ],
            ],
            'site_status' => [
                'label' => '站点状态',
                'default' => 1,
                'type' => 'bool',
                'data' => [0 => '关闭', 1 => '开启'],
            ],
            'site_close_reasons' => [
                'label' => '站点关闭原因',
                'type' => 'html',
                'hint' => '站点状态为关闭时的原因。',
                'inputOptions' => ['rows' => 5],
            ],
        ],
    ]
];
