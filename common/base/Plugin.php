<?php

namespace common\base;

use yii\base\Object;

abstract class Plugin extends Object implements \Serializable
{
    protected $meta = [];

    protected $config = [];

    private $_isInstalled = false;
    private $_isActive = false;

    public function getId() {
        return basename(dirname(self::className()));
    }

    public function getAuthor()
    {
        return isset($this->meta['author']) ? $this->meta['author'] : '未知';
    }

    public function getVersion()
    {
        return isset($this->meta['version']) ? $this->meta['version'] : '未知';
    }

    public function getDescription()
    {
        return isset($this->meta['description']) ? $this->meta['description'] : '';
    }

    public function getIsInstalled()
    {
        return $this->_isInstalled;
    }

    public function getIsActive()
    {
        return $this->_isActive;
    }

    public function setActive()
    {
        return $this->_isActive = true;
    }

    public function setInstalled()
    {
        $this->_isInstalled = true;
    }

    public function load()
    {
    }

    /**
     * 激活插件
     */
    public function activate()
    {
    }

    /**
     * 关闭插件
     */
    public function deactivate()
    {
    }

    /**
     * 安装插件
     */
    public function install()
    {
    }

    /**
     * 卸载插件
     */
    public function uninstall()
    {
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize([
            'meta' => $this->meta,
            'config' => $this->config,
            'isInstalled' => $this->_isInstalled,
            'isActive' => $this->_isInstalled,
        ]);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $array = unserialize($serialized);
        $this->meta = $array['meta'];
        $this->config = $array['config'];
        $this->_isInstalled = $array['isInstalled'];
        $this->_isActive = $array['isActive'];
    }
}
