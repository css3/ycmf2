<?php

namespace common\helper;


class Sanitizer
{
    public static function filename($filename)
    {
        $special_chars = array("?", "[", "]", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", chr(0));
        $filename = preg_replace('#\x{00a0}#siu', ' ', $filename);
        $filename = str_replace($special_chars, '', $filename);
        $filename = str_replace(array('%20', '+'), '-', $filename);
        $filename = preg_replace('/[\s-]+/', '-', $filename);
        return trim($filename, '.-_/\\');
    }
}
