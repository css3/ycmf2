<?php

namespace backend\models;


use yii\base\Model;

class PasswordForm  extends Model
{
    public $password;

    public $confirmPassword;

    public function rules()
    {
        return [
            [['password', 'confirmPassword'], 'string'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if ($this->password && !$this->confirmPassword) {
                $this->addError('confirmPassword', '确认密码不能为空。');
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'password' => '新密码',
            'confirmPassword' => '确认密码',
        ];
    }
}