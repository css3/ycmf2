<?php

namespace backend\models;


use common\models\File;
use yii\base\Model;
use Yii;

class FileSettingForm extends Model
{
    public $maxSize;

    public $extensions;

    public $thumbnailW;

    public $thumbnailH;

    public $thumbnailCrop;

    public $mediumW;

    public $mediumH;

    public $largeW;

    public $largeH;

    public $watermarkType;

    public $watermarkFontText;

    public $watermarkFontType;

    public $watermarkImageUrl;

    public $watermarkFontSize;

    public $watermarkFontColor;

    public $watermarkOpacity;

    public $watermarkPosition;

    public $watermarkDistanceX;

    public $watermarkDistanceY;

    public function rules()
    {
        return [
            [['watermarkImageUrl', 'watermarkFontText'], 'filter', 'filter' => 'trim'],
            ['maxSize', 'match', 'pattern' => '/^\d+\.?\d*[gmk]$/i'],
            ['watermarkFontColor', 'match', 'pattern' => '/^#[0-9abcdef]{6}/i'],
            [['extensions', 'watermarkFontText'], 'string'],
            ['watermarkImageUrl', 'url'],
            [['thumbnailW', 'thumbnailH', 'mediumW', 'mediumH', 'largeW', 'largeH',
                'watermarkFontSize', 'watermarkDistanceX', 'watermarkDistanceY'
            ], 'number', 'integerOnly' => true, 'min' => 0],
            ['watermarkType', 'in', 'range' => array_keys($this->watermarkTypeOptions())],
            ['watermarkFontType', 'in', 'range' => array_keys($this->watermarkFontTypeOptions())],
            ['watermarkPosition', 'in', 'range' => array_keys($this->watermarkPositionOptions())],
            ['watermarkOpacity', 'number', 'integerOnly' => true, 'min' => 0, 'max' => 100],
        ];
    }

    public function watermarkTypeOptions()
    {
        return [
            0 => '不设置',
            1 => '图片水印',
            2 => '文字水印',
        ];
    }

    public function watermarkFontTypeOptions()
    {
        return [
            '微软雅黑' => '微软雅黑',
        ];
    }

    public function watermarkPositionOptions()
    {
        return [
            'northWest' => '左上',
            'west' => '左中',
            'southWest' => '左下',
            'north' => '中上',
            'center' => '中部',
            'south' => '中下',
            'northEast' => '右上',
            'east' => '右中',
            'southEast' => '右下',
        ];
    }

    public function __construct($config = [])
    {
        $option = Yii::$app->get('option');
        $this->maxSize = $option->get('file.max_size', File::phpFileMaxSize());
        $this->extensions = File::fileExtensions();
        $imageSizes = File::imageSizes();
        if (isset($imageSizes[File::IMAGE_THUMBNAIL])) {
            $this->thumbnailW = $imageSizes[File::IMAGE_THUMBNAIL][0];
            $this->thumbnailH = $imageSizes[File::IMAGE_THUMBNAIL][1];
            $this->thumbnailCrop = $imageSizes[File::IMAGE_THUMBNAIL][2];
        }

        if (isset($imageSizes[File::IMAGE_MEDIUM])) {
            $this->mediumW = $imageSizes[File::IMAGE_MEDIUM][0];
            $this->mediumH = $imageSizes[File::IMAGE_MEDIUM][1];
        }

        if (isset($imageSizes[File::IMAGE_LARGE])) {
            $this->largeW = $imageSizes[File::IMAGE_LARGE][0];
            $this->largeH = $imageSizes[File::IMAGE_LARGE][1];
        }

        $this->watermarkType = $option->get('file.watermark_type', 0);
        $watermarkTypeOptions = $option->get('file.watermark_options', []);
        $watermarkTypeOptions = array_merge([
            'image_url' => '',
            'font_text' => '',
            'font_size' => 12,
            'font_color' => '#ffffff',
            'font_type' => '宋体',
            'opacity' => 100,
            'position' => 'center',
            'distance_x' => 10,
            'distance_y' => 10,
        ], $watermarkTypeOptions);

        $this->watermarkImageUrl = $watermarkTypeOptions['image_url'];
        $this->watermarkFontText = $watermarkTypeOptions['font_text'];
        $this->watermarkFontSize = $watermarkTypeOptions['font_size'];
        $this->watermarkFontType = $watermarkTypeOptions['font_type'];
        $this->watermarkFontColor = $watermarkTypeOptions['font_color'];
        $this->watermarkOpacity = $watermarkTypeOptions['opacity'];
        $this->watermarkPosition = $watermarkTypeOptions['position'];
        $this->watermarkDistanceX = $watermarkTypeOptions['distance_x'];
        $this->watermarkDistanceY = $watermarkTypeOptions['distance_y'];

        parent::__construct($config);
    }

    public function afterValidate()
    {
        parent::afterValidate();
        if ($this->watermarkType == 1) {
            if (!$this->watermarkImageUrl) {
                $this->addError('watermarkImageUrl', '水印图片地址不能为空');
            }
        } elseif ($this->watermarkType == 2) {
            if (!$this->watermarkFontText) {
                $this->addError('watermarkFontText', '水印文字不能为空');
            }
        }
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $watermarkTypeOptions = [
            'image_url' => $this->watermarkImageUrl,
            'font_text' => $this->watermarkFontText,
            'font_size' => $this->watermarkFontSize,
            'font_color' => $this->watermarkFontColor,
            'font_type' => $this->watermarkFontType,
            'opacity' => $this->watermarkOpacity,
            'position' => $this->watermarkPosition,
            'distance_x' => $this->watermarkDistanceX,
            'distance_y' => $this->watermarkDistanceY,
        ];

        $imageSizes = [
            File::IMAGE_THUMBNAIL => [$this->thumbnailW, $this->thumbnailH, $this->thumbnailCrop],
            File::IMAGE_MEDIUM => [$this->mediumW, $this->mediumH, false],
            File::IMAGE_LARGE => [$this->largeW, $this->largeH, false],
        ];

        /** @var \common\components\Option $option */
        $option = Yii::$app->get('option');
        $option->set('file.max_size', $this->maxSize, false);
        $option->set('file.extensions', $this->extensions, false);
        $option->set('file.image_sizes', $imageSizes, false);
        $option->set('file.watermark_type', $this->watermarkType, false);
        $option->set('file.watermark_options', $watermarkTypeOptions, false);
        $option->deleteCache();

        return true;
    }
}