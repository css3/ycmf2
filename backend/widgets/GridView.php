<?php

namespace backend\widgets;


use Yii;

class GridView extends \yii\grid\GridView
{
    public $batch;

    public $layout = "{batch}\n{summary}\n{items}\n{pager}";

    public function renderSection($name)
    {
        switch ($name) {
            case "{batch}":
                return $this->renderBatch();
            default:
                return parent::renderSection($name);
        }
    }

    public function renderBatch()
    {
        $count = $this->dataProvider->getCount();
        if ($count <= 0) {
            return '';
        }

        if (isset($this->batch) && is_array($this->batch)) {
            $this->batch['grid'] = $this;
            return BatchView::widget($this->batch);
        } else {
            return '';
        }
    }

}