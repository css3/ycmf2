jQuery(function($) {
   $('#plupload-upload-ui').find('.upload-desc a').click(function() {
       $(this).parents('form').addClass('html-uploader');
       $('#media-items').hide();
       setUserSetting('uploader', 1)
   });
   $('#html-upload-ui').find('.upload-desc a').click(function() {
       $(this).parents('form').removeClass('html-uploader');
       $('#media-items').show();
       deleteUserSetting('uploader');
   })
});