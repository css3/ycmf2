jQuery(function ($) {
    var $watermarkSettings = $('#watermark-settings');
    var $watermarkImageSettings = $('#watermark-image-settings');
    var $watermarkTextSettings = $('#watermark-text-settings');
    var $watermarkPosition = $('#watermarkPosition');
    var $watermarkDistanceX = $('#watermark-distance-x');
    var $watermarkDistanceY = $('#watermark-distance-y');

    $('#watermarkType').change(function () {
        var val = $(this).val();
        if (val == 0) {
            $watermarkSettings.hide();
        } else if (val == 1) {
            $watermarkSettings.show();
            $watermarkImageSettings.show();
            $watermarkTextSettings.hide();
        } else {
            $watermarkSettings.show();
            $watermarkTextSettings.show();
            $watermarkImageSettings.hide();
        }
    });

    $('#watermark-pos-select').on('click', 'li', function () {
        var $this = $(this);
        var val;
        if ($this.hasClass('left-top')) {
            val = 'northWest';
        } else if ($this.hasClass('middle-top')) {
            val = 'north';
        } else if ($this.hasClass('right-top')) {
            val = 'northEast';
        } else if ($this.hasClass('left-middle')) {
            val = 'west';
        } else if ($this.hasClass('middle-middle')) {
            val = 'center';
        } else if ($this.hasClass('right-middle')) {
            val = 'east';
        } else if ($this.hasClass('left-bottom')) {
            val = 'southWest';
        } else if ($this.hasClass('middle-bottom')) {
            val = 'south';
        } else if ($this.hasClass('right-bottom')) {
            val = 'southEast';
        }

        $watermarkPosition.val(val);

        if ($.inArray(val, ['center', 'north', 'south']) !== -1) {
            $watermarkDistanceX.hide();
        } else {
            $watermarkDistanceX.show();
        }

        if ($.inArray(val, ['center', 'west', 'east']) !== -1) {
            $watermarkDistanceY.hide();
        } else {
            $watermarkDistanceY.show();
        }
        $this.addClass('current').siblings().removeClass('current');
    })
});