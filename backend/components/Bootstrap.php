<?php
namespace backend\components;


use common\models\User;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\web\UserEvent;

class Bootstrap implements BootstrapInterface
{
    private $_app;

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        $app->params['admin.menu'] = $this->getAdminMenu();
        $app->getUser()->__set('on afterLogin', array($this, 'afterLogin'));
        $this->_app = $app;
    }

    /**
     * @param UserEvent $event
     */
    public function afterLogin($event)
    {
        $identity = $event->identity;
        $this->_app->session->set('perv_login', $identity->last_login);
        User::updateAll(['last_login' => time()], ['id' => $identity->getId()]);
    }

    public function getAdminMenu()
    {
        return [
            'items' => [
                'control_panel' => [
                    'priority' => 10,
                    'label' => '控制面板',
                    'icon' => 'dash-dashboard',
                    'url' => ['/site/index'],
                    'items' => [
                        ['label' => '首页', 'url' => ['/site/index']],
                    ],
                ],
                '_br',
                'user' => [
                    'priority' => 30,
                    'label' => '用户',
                    'icon' => 'dash-admin-users',
                    'url' => ['/user/index'],
                    'items' => [
                        ['label' => '所有用户', 'url' => ['/user/index']],
                        ['label' => '创建用户', 'url' => ['/user/create']],
                        ['label' => '角色', 'url' => ['/role/index']],
                        ['label' => '权限', 'url' => ['/permission/index']],
                    ],
                ],
                'media' => [
                    'priority' => 40,
                    'label' => '多媒体',
                    'icon' => 'dash-admin-media',
                    'url' => ['/file/index'],
                    'items' => [
                        ['label' => '媒体库', 'url' => ['/file/index']],
                        ['label' => '上传', 'url' => ['/file/upload']],
                    ]
                ],
                '_br',
                'settings' => [
                    'priority' => 80,
                    'label' => '设置',
                    'icon' => 'dash-admin-settings',
                    'url' => ['/site/settings', 'category' => 'general'],
                    'items' => [
                        ['label' => '常规', 'url' => ['/site/settings', 'category' => 'general']],
                        ['label' => '媒体选项', 'url' => ['/file/settings']],
                    ]
                ]
            ],
        ];
    }
}