<?php
namespace backend\assets;


use yii\web\AssetBundle;

class DashiconsAsset extends AssetBundle
{
    public $sourcePath = '@bower/dashicons';
    public $css = [
        'css/dashicons.min.css',
    ];
}