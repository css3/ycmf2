<?php

namespace backend\assets;


use yii\web\AssetBundle;
use Yii;

class PluploadAsset extends AssetBundle
{
    public $sourcePath = '@bower/plupload/js';
    public $js = [
        'plupload.full.min.js',
    ];

    public function init()
    {
        parent::init();
        if (Yii::$app->language === 'zh-CN') {
            $this->js[] = 'i18n/zh_CN.js';
        }
    }
}