<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CrystalAsset extends AssetBundle
{
    public $sourcePath = '@backend/source/crystal';
}
