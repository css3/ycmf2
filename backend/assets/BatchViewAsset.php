<?php

namespace backend\assets;


use yii\web\AssetBundle;

class BatchViewAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/ycmf.batchView.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}