<?php

namespace backend\assets;


use yii\web\AssetBundle;

class DataRangePickerAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-daterangepicker';
    public $css = [
        'daterangepicker-bs3.css',
    ];
    public $js = [
        'daterangepicker.js',
    ];
    public $depends = [
        'backend\assets\MomentAsset'
    ];
}