<?php

namespace backend\assets;


use yii\web\AssetBundle;

class ImageEditAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'js/imgareaselect/imgareaselect.css',
    ];
    public $js = [
        'js/imgareaselect/jquery.imgareaselect.js',
        'js/imageEdit.js',
    ];
    public $depends = [
        'yii\web\JQueryAsset',
    ];
}