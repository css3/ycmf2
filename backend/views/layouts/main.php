<?php
use backend\assets\AppAsset;
use backend\widgets\AdminMenu;
use backend\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Json;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$userSettings = [
    'url' => Yii::$app->getRequest()->baseUrl,
    'uid' => Yii::$app->getUser()->getId(),
    'time' => time(),
    'secure' => Yii::$app->getRequest()->getIsSecureConnection(),
];
/** @var $option \common\components\Option */
$option = Yii::$app->option;
$bodyClasses = [];
if ( $option->getUserSetting('mfold') == 'f') {
    $bodyClasses[] = 'folded';
}

if (!$option->getUserSetting('unfold')) {
    $bodyClasses[] = 'auto-fold';
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="robots" content="noindex,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> &lsaquo; <?= Html::encode(Yii::$app->option->get('general.site_title', Yii::$app->name)) ?> &#8212; <?= Html::encode(Yii::$app->name) ?></title>
    <?php $this->head() ?>
    <script>
    /* <![CDATA[ */
    var userSettings = <?= Json::encode($userSettings) ?>;
    /* ]]> */
    </script>
</head>
<body class="<?= implode(' ', $bodyClasses) ?>">
    <?php $this->beginBody() ?>
    <div id="wrap">
        <div id="admin-navbar">
                <?php
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-left primary-nav'],
                    'items' => [
                        ['label' => '<span class="dashicons dashicons-menu icon"></span> <span class="sr-only">菜单</span>',
                            'options' => ['id' => 'admin-menu-toggle'],
                            'encode' => false
                        ],
                        ['label' => '<span class="dashicons dashicons-admin-home icon"></span><span class="menu-name">' . Html::encode(Yii::$app->option->get('general.site_title', Yii::$app->name)) . '</span>', 'url' => Yii::$app->homeUrl, 'encode' => false],
                        ['label' => '<span class="dashicons dashicons-plus icon"></span> <span class="menu-name">创建</span>',
                            'items' => [
                                ['label' => '文章'],
                                ['label' => '页面'],
                                ['label' => '会员']
                            ],
                            'url' => Yii::$app->homeUrl, 'encode' => false],
                    ]
                ]);
                if (Yii::$app->user->isGuest) {
                    $menuItems[] = ['label' => '登录', 'url' => ['/site/login']];
                } else {
                    $menuItems[] = [
                        'label' => '您好，' . Yii::$app->user->identity->username,
                        'items' => [
                            ['label' => '编辑我的个人资料', 'url' => ['/user/profile']],
                            ['label' => '退出', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']],
                        ],
                        'options' => ['class' => 'account'],
                    ];
                }
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right secondary-nav'],
                    'items' => $menuItems,
                ]);
                ?>
        </div>
        <div id="admin-menu-wrap">
            <div id="admin-menu-back"></div>
            <div id="admin-menu">
                <?= AdminMenu::widget(Yii::$app->params['admin.menu']); ?>
            </div>
        </div>
        <div id="content">
            <div id="body" class="clearfix">
                <div id="body-content">
                    <div class="container-fluid">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget() ?>
                    <?= $content ?>
                    </div>
                </div>
             </div>
        </div>
        <div id="footer">
            <div class="container-fluid">
                <p class="pull-left">&copy; <?php echo date('Y'); ?></p>

                <p class="pull-right"><?php echo Yii::$app->version; ?> 版本</p>
            </div>
        </div>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
