<?php
use backend\assets\AppAsset;
use backend\widgets\Alert;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="robots" content="noindex,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> &lsaquo; <?= Html::encode(Yii::$app->option->get('general.site_title', Yii::$app->name)) ?> &#8212; <?= Html::encode(Yii::$app->name) ?></title>
    <?php $this->head() ?>
</head>
<body class="login">
<?php $this->beginBody() ?>
    <div class="login-wrap">
        <h1><?= Html::a(Html::encode(Yii::$app->option->get('general.site_title', Yii::$app->name)), ['site/login']) ?></h1>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

