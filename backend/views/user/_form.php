<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $passwordForm backend\models\PasswordForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'username')->textInput()->hint('必须唯一') ?>

    <?= $form->field($model, 'email')->textInput()->hint('必须唯一') ?>

    <?php if ($model->isNewRecord) {
        echo $form->field($model, 'password')->passwordInput();
    } else {
        echo $form->field($passwordForm, 'password')->passwordInput()->hint('留空不修改密码');
        echo $form->field($passwordForm, 'confirmPassword')->passwordInput();
    }?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatusList()) ?>

    <?= $form->field($model, 'roleNames')->checkboxList($model->getRoleList()) ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?= Html::submitButton($model->isNewRecord ? '创建' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
