<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $passwordForm backend\models\PasswordForm */

$this->title = '个人资料';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="user-form">

        <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
        <?= $form->field($model, 'username')->textInput()->hint('必须唯一') ?>

        <?= $form->field($model, 'email')->textInput()->hint('必须唯一') ?>

        <?= $form->field($passwordForm, 'password')->passwordInput()->hint('留空不修改密码') ?>

        <?= $form->field($passwordForm, 'confirmPassword')->passwordInput() ?>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <?= Html::submitButton('更新', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
