<?php

use backend\assets\NProgressAsset;
use backend\widgets\DatetimeColumn;
use backend\widgets\GridView;
use common\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\search\UserSearch */
/* @var $batchModel backend\models\UserBatch */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = '用户';
$this->params['breadcrumbs'][] = $this->title;

NProgressAsset::register($this);
?>
<div class="user-index">

    <h1>
        <?= Html::encode($this->title) ?>
        <?= Html::a('创建用户', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
    </h1>
    <?php Pjax::begin() ?>
    <?=
    GridView::widget([
        'batch' => [
            'model' => $batchModel,
            'columns' => [
                [
                    'name' => 'batch',
                    'label' => '应用',
                    'actions' => $batchModel->actions(),
                    'options' => ['prompt' => '批量操作'],
                    'confirm' => ['delete' => '你确定要删除这些数据吗？']
                ]
            ]
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn', 'options' => ['class' => 'col-checkbox'], 'name'=>'id'],
            ['attribute' => 'id', 'options' => ['class' => 'col-id']],
            'username',
            'email:email',
            ['attribute' => 'role', 'label' => '角色',  'format'=>'raw', 'value' => function ($model) {
                $roles = $model->getRoles();
                $output = [];
                foreach ($roles as $role) {
                    $output[] = Html::a(Html::encode($role->description), ['index', 'UserSearch[role]' => $role->name]);
                }
                return implode(',', $output);
            }],
            ['attribute' => 'status', 'value' => 'statusStr', 'filter' => User::getStatusList()],
            ['attribute' => 'created_at',
                'format' => 'date', 'class' => DatetimeColumn::className(),
                'dateRangeOptions' => [
                    'opens' => 'left',
                ],
                'empty' => '-'],
            ['attribute' => 'last_login',
                'format' => 'date', 'class' => DatetimeColumn::className(),
                'dateRangeOptions' => [
                    'opens' => 'left',
                ],
                'empty' => '从未登录'],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                            if ($model->id == Yii::$app->getUser()->id) {
                                $url = ['/user/profile'];
                            }
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Update'),
                                'data-pjax' => '0',
                            ]);
                        },
                    'delete' => function ($url, $model, $key) {
                            if ($model->id != \common\models\User::ROOT_ID || $model->id != Yii::$app->getUser()->id) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                        }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
