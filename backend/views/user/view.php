<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => '用户', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1>
        <?= Html::encode($this->title) ?>
        <?= Html::a('更新', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-sm btn-danger',
            'data' => [
                'confirm' => '确定要删除这条数据吗?',
                'method' => 'post',
            ],
        ]) ?>
    </h1>
    <?php
        $values = [];
        foreach ($model->getRoles() as $role) {
            $values[] = $role->description;
        }
        $roles = implode(', ', $values);
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email:email',
            'statusStr',
            [
                'attribute' => 'roles',
                'value' => $roles,
            ],
            'created_at:datetime',
            'updated_at:datetime',
            'last_login:datetime',
        ],
    ]) ?>

</div>
