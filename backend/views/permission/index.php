<?php

use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $roles yii\rbac\Role[] */

$this->title = '权限';
$this->params['breadcrumbs'][] = ['label' => '用户', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="permission-index">
    <h1>
        <?= Html::encode($this->title) ?>
        <?= Html::a('创建权限', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
    </h1>

    <?php
    $form = ActiveForm::begin(['action' => ['save'], 'id' => 'savePermissionsForm']);
    $columns = [
        [
            'attribute' => 'name',
            'label' => '名称',
            'content' => function($model) {
                    return '<div class="depth-' . $model['depth'] . '">' . '<code>' . $model['name'] . ' </code>'
                    . Html::a('<small class="glyphicon glyphicon-pencil"></small>', ['update', 'name' => $model['name']], ['title' => '更新权限“' . $model['description'] . '”']) .
                    (!$model['is_parent'] ?
                    Html::a('<small class="glyphicon glyphicon-trash"></small>', ['delete', 'name' => $model['name']], [
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]) : '')
                    . '<div class="description">'
                    . $model['description']
                    . '</div>'
                    . '</div>';
                }
        ],
    ];
    foreach ($roles as $role) {
        echo Html::hiddenInput('permission['. $role->name .']');
        $columns[] = [
            'label' => $role->description,
            'content' => function($model) use ($role) {
                if ($model['is_valid']) {
                    $check = in_array($role->name, $model['roles']);
                    return Html::checkbox('permission['. $role->name .'][]', $check, ['value' => $model['name'], 'data-role' => $role->name]);
                } else {
                    return '';
                }
            }
        ];
    }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'rowOptions' => function($model) {
            return ['data-parent' => $model['parent']];
        }
    ]); ?>
    <?= Html::submitButton('保存', ['class' =>'btn btn-primary']) ?>
    <?php ActiveForm::end() ?>
</div>
<?php
$js = <<<EOT
(function($) {
var form = $('#savePermissionsForm');
form.find('input:checkbox').click(function() {
    var value = $(this).val(),
        role = $(this).data('role'),
        parent;
    if (this.checked) {
        form.find('tr').each(function() {
            if ($(this).data('parent') === value) {
                $(this).find('input[data-role="' + role + '"]').prop('checked', true);
            }
        });
    } else {
        parent = $(this).parents('tr');
        $('input[value="' + parent.data('parent') + '"]').prop('checked', false);
    }
});
})(jQuery);
EOT;
$this->registerJs($js);
