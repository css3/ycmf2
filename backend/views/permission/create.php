<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PermissionForm */

$this->title = '创建权限';
$this->params['breadcrumbs'][] = ['label' => '用户', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = ['label' => '权限', 'url' => ['/permission/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="permission-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
