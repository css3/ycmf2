<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SettingForm */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = $this->title;
$options = $model->getOptions();
?>
<div class="site-settings">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($options) { ?>
    <div class="setting-form">

        <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

        <?php
        foreach ($options as $name => $params) {
            $type = isset($params['type']) ? $params['type'] : 'text';
            $inputOptions = isset($params['inputOptions']) ? $params['inputOptions'] : [];

            $field = $form->field($model, $name);
            if (isset($params['hint'])) {
                $field->hint($params['hint']);
            }

            switch ($type) {
                case 'html':
                case 'textarea':
                    $field->textarea($inputOptions);
                    break;
                case 'select':
                    $field->dropDownList(isset($params['data']) ? $params['data']: [], $inputOptions);
                    break;
                case 'radio':
                    if (isset($params['data']) && is_array($params['data'])) {
                        $field->radioList($params['data'], $inputOptions);
                    } else {
                        $field->radio($inputOptions);
                    }
                    break;
                case 'checkbox':
                    if (isset($params['data']) && is_array($params['data'])) {
                        $field->checkboxList($params['data'], $inputOptions);
                    } else {
                        $field->checkbox($inputOptions);
                    }
                    break;
                case 'boolean':
                case 'bool':
                    $field->radioList($params['data'], $inputOptions);
                    break;
                case 'image':
                case 'file':
                    $field->fileInput($inputOptions);
                    break;
                default:
                    $field->textInput($inputOptions);
            }
            echo $field;
        }
        ?>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <?= Html::submitButton('保存', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <?php } ?>
</div>
