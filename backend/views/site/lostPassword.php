<?php
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\LostPasswordForm */

$this->title = '忘记密码';
?>
<div class="site-lost-password">
    <?= Alert::widget(['body' => '请输入您的电子邮箱地址。您会收到一封包含创建新密码链接的电子邮件。',
        'options' => ['class' => 'alert alert-info']]) ?>
    <?php $form = ActiveForm::begin(['id' => 'lost-password-form']); ?>
        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
        <div class="form-group">
            <?= Html::submitButton('发送', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    <ul class="login-nav">
        <li><?= Html::a('登录', ['/site/login']) ?></li>
    </ul>
</div>
