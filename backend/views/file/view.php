<?php

use common\models\File;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\File */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = ['label' => '媒体库', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-view">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('更新', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-sm btn-danger',
            'data' => [
                'confirm' => '确定要删除这条数据吗?',
                'method' => 'post',
            ],
        ]) ?>
    </h1>

    <?php if ($model->isImage()){ ?>
        <div class="thumbnail">
            <?= $model->getImage(File::IMAGE_ORIGIN) ?>
        </div>
    <?php } ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user',
                'value' => $model->user ? $model->user->username : null,
            ],
            [
                'attribute' => 'name',
                'value' => $model->name . '.' . strtoupper($model->getExtension()),
            ],
            [
                'attribute' => 'file_size',
                'format' => 'size',
            ],
            'caption',
            'description',
            'path',
            'url',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
</div>
