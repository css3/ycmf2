<?php

use qiniu\Form;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\File */

$this->title = '上传文件';
$this->params['breadcrumbs'][] = ['label' => '媒体库', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$hint = '允许上传的文件类型：' . $model->getFileExtensionsByField('file') . '。最大大小为：' . $model->getFileMaxSizeByField('file');
?>
<div class="file-upload">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="file-form">
        <?php
        $form = form::begin([
            'putPolicy'=>[
                'scope' => 'css3-test',
                'returnUrl' => url::to(['index'], true),
                'saveKey' => date('y/m/d') . '/' . time() . '$(ext)',
                'extensions' =>  $model->getFileExtensionsByField('file'),
            ],
        ]);
        ?>

        <?= Html::fileInput('file') ?>
        <div class="hint">
        <?= $hint ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('上传', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php Form::end(); ?>

    </div>
</div>
