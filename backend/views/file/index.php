<?php

use backend\assets\NProgressAsset;
use backend\widgets\BatchView;
use backend\widgets\DatetimeColumn;
use backend\widgets\GridView;
use common\models\File;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\File */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\search\FileSearch */

$this->title = '媒体库';
$this->params['breadcrumbs'][] = $this->title;

NProgressAsset::register($this);
?>
<div class="file-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('上传', ['upload'], ['class' => 'btn btn-sm btn-primary']) ?>
    </h1>
    <?php Pjax::begin() ?>
    <?=
    GridView::widget([
        'batch' => [
            'model' => $batchModel,
            'columns' => [
                [
                    'name' => 'batch',
                    'label' => '应用',
                    'actions' => ['delete' => '删除'],
                    'options' => ['prompt' => '批量操作'],
                    'confirm' => ['delete' => '你确定要删除这些数据吗？']
                ]
            ]
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn', 'options' => ['class' => 'col-checkbox'], 'name' => 'id'],
            ['attribute' => 'id', 'options' => ['class' => 'col-id']],
            [
            'options' => ['class' => 'col-media-icon'],
            'format' => 'raw',
            'contentOptions' => ['class' => 'col-media-icon'],
            'value' => function ($model) {
                $options['alt'] = $model->getName();
                if ($model->isImage()) {
                    $thumbnailSize = $model->getImageSizeByName(File::IMAGE_THUMBNAIL);
                    if ($thumbnailSize) {
                        $options['height'] = 60;
                        $options['width'] = floor(60 / $thumbnailSize['height'] * $thumbnailSize['width']);
                    } elseif ($model->getHeight() && $model->getWidth()) {
                        $options['height'] = 60;
                        $options['width'] = floor(60 / $model->getHeight() * $model->getWidth());
                    }
                } else {
                    $options['width'] = 48;
                    $options['height'] = 64;
                }
                return Html::img($model->getIconUrl(), $options);
            }],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->name), ['update', 'id'=>$model->id], ['data-pjax' => '0'])
                        . '<p>' . strtoupper($model->getExtension()) . '</p>';
                }
            ],
            [
                'attribute' => 'user',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->user ? Html::a(Html::encode($model->user->username), ['index', 'FileSearch[user]'=>$model->user->username]) : null;
                }
            ],
            [
                'attribute' => 'file_size',
                'format' => 'shortSize',
            ],
            ['attribute' => 'created_at',
                'format' => 'date', 'class' => DatetimeColumn::className(),
                'dateRangeOptions' => [
                    'opens' => 'left',
                ],
                'empty' => '-'],
            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
