<?php
use common\models\File;
use yii\helpers\Url;

/** @var integer $id */
/** @var integer $width */
/** @var integer $height */
/** @var integer $thumbW */
/** @var integer $thumbH */
/** @var bool $canRestore */
/** @var \common\models\File $model */
/** @var integer $sizer */

$note = '';
if ( isset($result) ) {
    if ( isset($result['error']) )
        $note = "<div class='alert alert-error'>{$result['error']}</div>";
    elseif ( isset($result['msg']) )
        $note = "<div class='alert alert-success'>{$result['msg']}</div>";
}
$imageSizes = File::imageSizes();
if (isset($imageSizes[File::IMAGE_THUMBNAIL])) {
    $minthumb = $imageSizes[File::IMAGE_THUMBNAIL][0] . ':' . $imageSizes[File::IMAGE_THUMBNAIL][1];
} else {
    $minthumb = '128:128';
}
?>
<div class="imgedit-wrap">
    <div id="imgedit-panel-<?= $id ?>" class="clearfix">
        <div class="imgedit-settings">
            <div class="panel panel-default imgedit-group">
                <div class="panel-heading">
                    拉伸图像 <a href="#" class="dashicons dashicons-editor-help imgedit-help-toggle"></a>
                </div>

                <div class="panel-body">
                    <div class="imgedit-help">
                        <p>您可以成比例地拉伸原始图片，在诸如裁切、旋转的编辑操作之前，最好先伸缩调整好您的图片尺寸。图片仅能被缩小，不能被放大。</p>
                    </div>
                    <p>原始尺寸<?= $width ?> &times; <?= $height ?></p>

                    <div class="imgedit-submit">
                    <span class="nowrap">
                        <input type="text" id="imgedit-scale-width-<?= $id ?>" style="width:4em;" value="<?= $width ?>"/> &times;
                        <input type="text" id="imgedit-scale-height-<?= $id ?>" style="width:4em;" value="<?= $height ?>"/>
                        <span class="imgedit-scale-warn" id="imgedit-scale-warn-<?= $id ?>">!</span>
                    </span>
                        <input type="button" class="btn btn-sm btn-primary imgedit-scale-btn" value="拉伸"/>
                    </div>
                </div>
            </div>
            <?php if ($canRestore) { ?>
                <div class="panel panel-default imgedit-group">
                    <div class="panel-heading">
                        <a href="#" class="imgedit-help-toggle">还原原始图像 <span class="dashicons dashicons-arrow-down"></span></a>
                    </div>

                    <div class="panel-body">
                        <div class="imgedit-help">
                            <p>放弃所有变更，并恢复到原始图片。 先前编辑过的图片副本不会被删除。</p>
                        </div>
                        <div class="imgedit-submit">
                            <input type="button" value="还原图像" class="btn btn-sm btn-primary imgedit-restore-btn">
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="panel panel-default imgedit-group">
                <div class="panel-heading">
                    图像裁切 <a href="#" class="dashicons dashicons-editor-help imgedit-help-toggle"></a>
                </div>
                <div class="panel-body">

                    <div class="imgedit-help">
                        <p>要裁切此图像，点击并拖动来确认您的选择。</p>

                        <p><strong>按比例裁切</strong><br/>
                            纵横比是宽与高之间的比值。您可以通过在修改选区时按住Shift键来固定纵横比。使用输入框来指定纵横比，如1:1（方形）、4:3、16:9等。</p>

                        <p><strong>裁切选区</strong><br/>
                            您做出选择后，选区大小可以通过输入像素值值来调整。最小值为媒体设置中指定的缩略图大小。</p>
                    </div>

                    <p> 长宽比： <span class="nowrap">
                        <input type="text" id="imgedit-crop-width-<?= $id ?>" style="width:3em;"/>
                        : <input type="text" id="imgedit-crop-height-<?= $id ?>" style="width:3em;"/>
                    </span>
                    </p>

                    <p id="imgedit-crop-sel-<?= $id ?>"> 选区：
                    <span class="nowrap"> <input type="text" id="imgedit-sel-width-<?= $id ?>" style="width:4em;"/>
                        &times;
                        <input type="text" id="imgedit-sel-height-<?= $id ?>" style="width:4em;"/>
                    </span>
                    </p>
                </div>
            </div>


            <?php if ($model->hasImage(File::IMAGE_THUMBNAIL)) { ?>
            <div class="panel panel-default imgedit-group imgedit-applyto">
                <div class="panel-heading">
                    缩略图设置 <a href="#" class="dashicons dashicons-editor-help imgedit-help-toggle"></a>
                </div>

                <div class="panel-body">
                    <p class="imgedit-help">您可以编辑图像，并无需影响缩略图。比如，您可能希望有一张只展示图像一部分的方形缩略图。</p>

                    <p>
                        <img src="<?= $model->getImageUrl(File::IMAGE_THUMBNAIL) ?>" width="<?= $thumbW ?>"
                             height="<?= $thumbH ?>" class="imgedit-size-preview" alt="<?= $model->getAlt(); ?>"
                             draggable="false"/>
                        <br/>当前缩略图 </p>

                    <p id="imgedit-save-target-<?= $id ?>">
                        <strong>将更改应用于：</strong><br/>

                        <label class="imgedit-label">
                            <input type="radio" name="imgedit-target-<?= $id ?>" value="all" checked="checked"/>
                            所有图像大小
                        </label>

                        <label class="imgedit-label">
                            <input type="radio" name="imgedit-target-<?= $id ?>" value="thumbnail"/>
                            缩略图
                        </label>

                        <label class="imgedit-label">
                            <input type="radio" name="imgedit-target-<?= $id ?>" value="nothumb"/>
                            除缩略图外所有尺寸
                        </label>
                    </p>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="imgedit-panel-content">
            <?= $note; ?>
            <div class="imgedit-menu">
                <button class="btn disabled imgedit-crop" title="裁切">
                    <span class="dashicons dashicons-image-crop"></span>
                </button>
                <button class="btn imgedit-rleft" title="逆时针旋转">
                    <span class="dashicons dashicons-image-rotate-left"></span>
                </button>
                <button class="btn imgedit-rright" title="顺时针旋转">
                    <span class="dashicons dashicons-image-rotate-right"></span>
                </button>
                <button class="btn imgedit-flipv" title="垂直翻转">
                    <span class="dashicons dashicons-image-flip-vertical"></span>
                </button>
                <button class="btn imgedit-fliph" title="水平翻转">
                    <span class="dashicons dashicons-image-flip-horizontal"></span>
                </button>
                <button class="btn disabled imgedit-undo" title="撤销" id="image-undo-<?= $id ?>">
                    <span class="dashicons dashicons-undo"></span>
                </button>
                <button class="btn disabled imgedit-redo" title="重做" id="image-redo-<?= $id ?>">
                    <span class="dashicons dashicons-redo"></span>
                </button>
            </div>

            <input type="hidden" id="imgedit-sizer-<?= $id ?>" value="<?= $sizer ?>"/>
            <input type="hidden" id="imgedit-minthumb-<?= $id ?>" value="<?= $minthumb ?>"/>
            <input type="hidden" id="imgedit-history-<?= $id ?>" value=""/>
            <input type="hidden" id="imgedit-undone-<?= $id ?>" value="0"/>
            <input type="hidden" id="imgedit-selection-<?= $id ?>" value=""/>
            <input type="hidden" id="imgedit-x-<?= $id ?>" value="<?= $width ?>"/>
            <input type="hidden" id="imgedit-y-<?= $id ?>" value="<?= $height ?>"/>

            <div id="imgedit-crop-<?= $id ?>" class="imgedit-crop-wrap">
                <img id="image-preview-<?= $id ?>" onload="imageEdit.imgLoaded()" src="<?= Url::to(['file/imgedit-preview', 'id'=>$id, 'rand'=>rand(1, 9999)]); ?>">
            </div>

            <div class="imgedit-submit">
                <input type="button" class="btn btn-default imgedit-close" value="取消"/>
                <input type="button" disabled class="btn btn-primary imgedit-submit-btn" value="保存"/>
            </div>
        </div>
    </div>
    <div class="imgedit-wait" id="imgedit-wait-<?= $id ?>"></div>
    <script type="text/javascript">
        jQuery(function () {
            imageEdit.init(<?= $id ?>);
        });
    </script>
    <div class="hidden" id="imgedit-leaving-<?= $id ?>">未保存的更改将丢失。按“确定”以继续，按“取消”可返回“图片编辑器”。</div>
</div>
