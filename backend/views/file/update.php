<?php

use common\models\File;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\File */

$this->title = '更新文件';
$this->params['breadcrumbs'][] = ['label' => '媒体库', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-update">
    <h1><?= Html::encode($this->title) ?> </h1>
    <?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-sm-8">
            <?php if ($model->isImage()){ ?>
                <div class="imgedit-response" id="imgedit-response-<?= $model->id ?>"></div>
                <div id="image-editor-<?= $model->id ?>"></div>
                <div id="image-head-<?= $model->id ?>">
                    <div class="thumbnail">
                    <?= $model->getImage(File::IMAGE_ORIGIN) ?>
                    </div>
                    <p> <?= Html::button('编辑图片', [
                            'id' => 'edit-image-btn',
                            'class' => 'btn btn-sm btn-default',
                            'data-loading-text' => 'Loading...',
                            'autocomplete' => 'off'
                        ]) ?> </p>
                </div>
            <?php } ?>

            <?= $form->field($model, 'name')->textInput() ?>

            <?php
            if ($model->isImage()) {
                echo $form->field($model, 'alt')->textInput();
            }
            ?>

            <?= $form->field($model, 'caption')->textarea(['rows' => 2]) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
            <div class="form-group">
                <?= Html::submitButton('更新', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">文件信息</div>
                <div class="panel-body">
                    <p><span class="fa fa-calendar"></span> 上传于：<?= Yii::$app->formatter->asDatetime($model->created_at) ?> </p>
                    <p> 文件URL：
                        <?= Html::textInput('url', $model->getUrl(), ['readonly' => true, 'class' => 'form-control clone']) ?></p>
                    <p>文件名：<strong><?= $model->file_name ?></strong></p>
                    <p>文件类型：<strong><?= strtoupper($model->getExtension()) ?></strong></p>
                    <p>文件大小：<strong><?= Yii::$app->formatter->asShortSize($model->file_size) ?></strong></p>
                    <?php if ($model->isImage()) { ?>
                        <p>文件尺寸：<strong id="media-dims-<?= $model->id ?>"><?= $model->getWidth() ?>&nbsp;&times;&nbsp;<?= $model->getHeight() ?></strong> </p>
                    <?php } ?>
                </div>
                <div class="panel-footer">
                    <?= Html::a('删除', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => '确定要删除这条数据吗?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>
<?php
$options = Json::encode([
   'url' => Url::to(['/file/image-editor', 'id' => $model->id]),
]);
if ($model->isImage()) {
    $js = <<<EOT
    jQuery('#edit-image-btn').click(function() {
        var btn = $(this).button('loading');
        jQuery.ajax({$options})
        .done(function(resp) {
            jQuery('#image-editor-{$model->id}').html(resp).fadeIn('fast');
            jQuery('#image-head-{$model->id}').fadeOut('fast');
        })
        .always(function() {
            btn.button('reset');
        });
    });
EOT;
$this->registerJs($js);
\backend\assets\ImageEditAsset::register($this);
}
