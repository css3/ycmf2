<?php

use backend\widgets\Plupload;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\File */

$this->title = '上传文件';
$this->params['breadcrumbs'][] = ['label' => '媒体库', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$hint = '允许上传的文件类型：' . $model->getFileExtensionsByField('file') . '。最大大小为：' . $model->getFileMaxSizeByField('file');
$uploader = Yii::$app->get('option')->getUserSetting('uploader');
$this->registerJsFile('@web/js/upload.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="file-upload">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="file-form">
        <?php
            $form = ActiveForm::begin([
                'layout' => 'inline',
                //'enableClientScript' => false,
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => $uploader ? 'html-uploader' : '',
                ]
            ]);
        ?>

        <div id="plupload-upload-ui">
            <?= Plupload::widget([
                'model' => $model,
                'attribute' => 'file',
            ]) ?>
            <p class="upload-desc">
                您正在使用多文件上传工具。遇到问题？请尝试使用<a href="#">标准浏览器上传工具</a> 。
            </p>
        </div>
        <div id="html-upload-ui">
            <div class="upload-wrap">
            <?= $form->field($model, 'file', ['enableError' => true])->fileInput() ?>
            <?= Html::submitButton('上传', ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
            <p class="upload-desc">
                您正在使用浏览器内置的标准上传工具。使用全新的上传工具，支持多选、拖放上传功能。改用<a href="#">新的上传工具</a>。
            </p>
        </div>
        <div class="help-block">
            <?= $hint ?>
        </div>
        <div id="media-items" class="media-items"></div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
