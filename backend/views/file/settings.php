<?php

use backend\assets\AppAsset;
use common\models\File;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FileSettingForm */

$this->title = '媒体选项';
$this->params['breadcrumbs'][] = ['label' => '设置', 'url' => ['/site/settings', 'category' => 'general']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('@web/js/fileSettings.js', ['depends' => AppAsset::className()]);
?>
<div class="file-settings">
    <h1><?= Html::encode($this->title) ?> </h1>
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']) ?>
    <fieldset>
        <legend>媒体</legend>
        <div class="form-group">
            <div class="col-sm-2">大小</div>
            <div class="col-sm-6">
                <label><?= Html::activeInput('text', $model, 'maxSize', ['style' => 'width: 4em;']) ?></label>
                <?= Html::error($model, 'maxSize') ?>
                <p>允许上传文件的最大大小，系统默认上传文件的最大大小为 <code><?= File::phpFileMaxSize() ?></code></p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2">类型</div>
            <div class="col-sm-6">
                <label><?= Html::activeInput('text', $model, 'extensions', ['style' => 'width: 20em;']) ?></label>

                <p>
                    允许上传媒体的类型，多个以英文逗号 <code>,</code> 分隔开来
                </p>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>图像大小</legend>
        <p>下面列出来的尺寸决定插入媒体库内的图像之最大尺寸。以像素为单位。</p>

        <div class="form-group">
            <div class="col-sm-2">缩略图大小</div>
            <div class="col-sm-6">
                <label>宽度 <?= Html::activeInput('number', $model, 'thumbnailW', ['style' => 'width:4em', 'step' => 1, 'min' => 0]) ?></label>
                <label>高度 <?= Html::activeInput('number', $model, 'thumbnailH', ['style' => 'width:4em', 'step' => 1, 'min' => 0]) ?></label>
                <br/>
                <label>
                    <?= Html::activeCheckbox($model, 'thumbnailCrop', ['label' => false]) ?>
                    总是裁剪缩略图到这个尺寸（一般情况下，缩略图应保持原始比例）
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2">中等大小</div>
            <div class="col-sm-6">
                <label>最大宽度 <?= Html::activeInput('number', $model, 'mediumW', ['style' => 'width:4em', 'step' => 1, 'min' => 0]) ?></label>
                <label>最大高度 <?= Html::activeInput('number', $model, 'mediumH', ['style' => 'width:4em', 'step' => 1, 'min' => 0]) ?></label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2">大尺寸</div>
            <div class="col-sm-6">
                <label>最大宽度 <?= Html::activeInput('number', $model, 'largeW', ['style' => 'width:4em', 'step' => 1, 'min' => 0]) ?></label>
                <label>最大高度 <?= Html::activeInput('number', $model, 'largeH', ['style' => 'width:4em', 'step' => 1, 'min' => 0]) ?></label>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>图片水印</legend>
        <div class="form-group">
            <div class="col-sm-2">水印</div>
            <div class="col-sm-6">
                <?= Html::activeDropDownList($model, 'watermarkType', $model->watermarkTypeOptions(), ['id' => 'watermarkType']) ?>
            </div>
        </div>
        <div id="watermark-settings" style="display:<?= $model->watermarkType != 0 ? 'block' : 'none' ?>">
            <div id="watermark-image-settings" style="display:<?= $model->watermarkType == 1 ? 'block' : 'none' ?>">
                <div class="form-group required <?= $model->hasErrors('watermarkImageUrl') ? 'has-error' : '' ?>">
                    <div
                        class="col-sm-2"><?= Html::label('水印图片', Html::getInputId($model, 'watermarkImageUrl')) ?></div>
                    <div class="col-sm-6">
                        <?= Html::activeInput('text', $model, 'watermarkImageUrl', ['placeholder' => '请输入图片URL']) ?>
                        <?= Html::error($model, 'watermarkImageUrl', ['class' => 'help-block help-block-error']) ?>
                    </div>
                </div>
            </div>
            <div id="watermark-text-settings" style="display:<?= $model->watermarkType == 2 ? 'block' : 'none' ?>">
                <div class="form-group required <?= $model->hasErrors('watermarkFontText') ? 'has-error' : '' ?>">
                    <div
                        class="col-sm-2"><?= Html::label('水印文字', Html::getInputId($model, 'watermarkFontText')) ?></div>
                    <div class="col-sm-6">
                        <?= Html::activeInput('text', $model, 'watermarkFontText') ?>
                        <?= Html::error($model, 'watermarkFontText', ['class' => 'help-block help-block-error']) ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2">水印字体</div>
                    <div class="col-sm-6">
                        <?= Html::activeDropDownList($model, 'watermarkFontType', $model->watermarkFontTypeOptions()) ?>
                        <label>字体大小 <?= Html::activeInput('number', $model, 'watermarkFontSize', ['style' => 'width:4em', 'step' => 1, 'min' => 0]) ?>
                            pt</label>
                        <label>字体颜色 <?= Html::activeInput('text', $model, 'watermarkFontColor', ['style' => 'width: 4em']) ?></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">不透明度</div>
                <div class="col-sm-6">
                    <?= Html::activeInput('number', $model, 'watermarkOpacity', ['style' => 'width:4em', 'step' => 1, 'min' => 0, 'max' => 100]) ?>
                    %
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">显示位置</div>
                <div class="col-sm-6">
                    <?php $pos = $model->watermarkPosition ?>
                    <ul class="pos-select" id="watermark-pos-select">
                        <li class="left-top <?= $pos == 'northWest' ? 'current' : '' ?>">左上</li>
                        <li class="middle-top <?= $pos == 'north' ? 'current' : '' ?>">中上</li>
                        <li class="right-top <?= $pos == 'northEast' ? 'current' : '' ?>">右上</li>
                        <li class="left-middle <?= $pos == 'west' ? 'current' : '' ?>">左中</li>
                        <li class="middle-middle <?= $pos == 'center' ? 'current' : '' ?>">中部</li>
                        <li class="right-middle <?= $pos == 'east' ? 'current' : '' ?>">右中</li>
                        <li class="left-bottom <?= $pos == 'southWest' ? 'current' : '' ?>">左下</li>
                        <li class="middle-bottom <?= $pos == 'south' ? 'current' : '' ?>">中下</li>
                        <li class="right-bottom <?= $pos == 'southEast' ? 'current' : '' ?>">右下</li>
                    </ul>
                    <?= Html::activeInput('hidden', $model, 'watermarkPosition', ['id' => 'watermarkPosition']); ?>
                    <div class="pos-input pull-right">
                        <div id="watermark-distance-x"
                             style="display: <?= in_array($pos, ['center', 'north', 'south']) ? 'none' : 'block' ?>">
                            <label>横向边距(px) <?= Html::activeInput('number', $model, 'watermarkDistanceX', ['style' => 'width:4em', 'step' => 1, 'min' => 0]) ?></label>
                        </div>
                        <div id="watermark-distance-y"
                             style="display: <?= in_array($pos, ['center', 'west', 'east']) ? 'none' : 'block' ?>">
                            <label>纵向边距(px) <?= Html::activeInput('number', $model, 'watermarkDistanceY', ['style' => 'width:4em', 'step' => 1, 'min' => 0]) ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="form-group">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-primary">保存</button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
