<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\RoleForm */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="role-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'name')->textInput()->hint('只能包含小写英文字母，且唯一') ?>

    <?= $form->field($model, 'description')->textInput() ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?= Html::submitButton($model->isNew ? '创建' : '更新', ['class' => $model->isNew ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
