<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '角色';
$this->params['breadcrumbs'][] = ['label' => '用户', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-index">
    <h1>
        <?= Html::encode($this->title) ?>
        <?= Html::a('创建角色', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'label' => '名称',
                'content' => function($model) {
                    return Html::a($model->name, ['/role/update', 'name' => $model->name], ['title' => '更新角色“' . $model->name . '”']);
                }
            ],
            ['attribute' => 'description', 'label' => '描述'],
            [
                'label' => '',
                'content' => function($model) {
                    return Html::a('更新权限', ['/permission', 'name' => $model->name]);
                }
            ],
            [
                'label' => '',
                'content' => function($model) {
                    return Html::a('删除', ['delete', 'name' => $model->name], [
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
                }
            ],
        ],
    ]); ?>
</div>
