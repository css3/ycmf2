<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\RoleForm */

$this->title = '创建角色';
$this->params['breadcrumbs'][] = ['label' => '用户', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = ['label' => '角色', 'url' => ['/role/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
